# Demystifying the Magic - Debugging in Ruby and Rails #

![Demystifying the Magic- Debugging in Ruby and Rails.jpg](https://bitbucket.org/repo/EqXzdd/images/2494106935-Demystifying%20the%20Magic-%20Debugging%20in%20Ruby%20and%20Rails.jpg)

This is the exported slide deck for a talk I gave at the [Nebraska.code() conference](http://nebraskacode.com/) on using [Pry](https://github.com/pry/pry) and some other gems to drastically improve the debugging and code exploration process for Ruby and the Rails web framework.

Gems referenced in the talk but not in the slides:

1. [hirb](https://github.com/cldwalker/hirb)
1. [awesome_print](https://github.com/michaeldv/awesome_print)
1. [pry-rails](https://github.com/rweng/pry-rails)
1. [pry-rescue](https://github.com/ConradIrwin/pry-rescue)